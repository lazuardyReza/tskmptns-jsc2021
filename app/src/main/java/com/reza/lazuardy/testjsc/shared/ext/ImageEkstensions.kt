package id.damcorp.baseapp.ekstensions

import android.content.Context
import androidx.core.content.ContextCompat
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.MemoryCategory
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import id.damcorp.baseapp.R
import id.damcorp.baseapp.supportclasses.RoundedCornersTransformation

fun loadImage(context: Context,
              url: Any,
              imageView: ImageView,
              placeHolder: Int) {

    fun setMemoryCategory(context: Context) {
        Glide.get(context).setMemoryCategory(MemoryCategory.NORMAL)
    }

    setMemoryCategory(context)
    GlideApp.with(context)
            .load(url)
            .centerCrop()
            .dontAnimate()
            .placeholder(placeHolder)
            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
            .into(imageView)
}

fun loadImageDrawable(context: Context,
              url: Any,
              imageView: ImageView,
              placeHolder: Int) {

    fun setMemoryCategory(context: Context) {
        Glide.get(context).setMemoryCategory(MemoryCategory.NORMAL)
    }

    setMemoryCategory(context)
    GlideApp.with(context)
            .load(url)
            .centerCrop()
            .dontAnimate()
            .placeholder(placeHolder)
            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
            .into(imageView)
}

fun loadImageRounded(context: Context,
                     url: Any, radius: Int, margin: Int,
                     imageView: ImageView,
                     placeHolder: Int) {

    fun setMemoryCategory(context: Context) {
        Glide.get(context).setMemoryCategory(MemoryCategory.NORMAL)
    }

    setMemoryCategory(context)
    GlideApp.with(context)
            .load(url)
            .fitCenter()
            .dontAnimate()
            .apply(RequestOptions.bitmapTransform(RoundedCornersTransformation(radius, margin,
                    RoundedCornersTransformation.CornerType.ALL)))
            .placeholder(placeHolder)
            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
            .into(imageView)
}

fun loadImageRoundedCenterCrop(context: Context,
                               url: Any, radius: Int, margin: Int,
                               imageView: ImageView,
                               placeHolder: Int) {

    fun setMemoryCategory(context: Context) {
        Glide.get(context).setMemoryCategory(MemoryCategory.NORMAL)
    }

    setMemoryCategory(context)
    GlideApp.with(context)
            .load(url)
            .centerCrop()
            .dontAnimate()
            .apply(RequestOptions.bitmapTransform(RoundedCornersTransformation(radius, margin,
                    RoundedCornersTransformation.CornerType.ALL)))
            .placeholder(placeHolder)
            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
            .into(imageView)
}