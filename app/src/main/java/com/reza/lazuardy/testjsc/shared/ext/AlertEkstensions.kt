@file:JvmName("BaseAlert")
package id.damcorp.baseapp.ekstensions

import android.app.Activity
import android.content.Context
import android.widget.Toast

/**
 * Created by ramadhan on 2/27/18.
 */

fun Activity.toastLong(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}
fun Context.toastLong(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}
fun Activity.toastShort(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}
fun Context.toastShort(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}