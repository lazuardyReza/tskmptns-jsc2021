package com.reza.lazuardy.testjsc.screen.contact

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class ContactActivityView : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_view)
    }
}