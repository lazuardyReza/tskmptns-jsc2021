package com.reza.lazuardy.testjsc.model

import kotlinx.android.parcel.Parcelize
import android.os.Parcelable
import eu.amirs.JSON
import id.damcorp.baseapp.ekstensions.getKey
import id.damcorp.baseapp.ekstensions.getList
import id.damcorp.baseapp.ekstensions.getString


@Parcelize
data class NameModel (
    var title:String="",
    var first:String="",
    var last:String=""
) : Parcelable {
    constructor(data: JSON) : this() {
        this.title = data.getString("title")
        this.first = data.getString("first")
        this.last =  data.getString("last")
    }
}