package com.reza.lazuardy.testjsc.model

import kotlinx.android.parcel.Parcelize
import android.os.Parcelable
import eu.amirs.JSON
import id.damcorp.baseapp.ekstensions.getString


@Parcelize
data class PictureModel (
    var large:String="",
    var medium:String="",
    var thumbnail: String=""
) : Parcelable {
    constructor(data: JSON) : this() {
        this.large = data.getString("large")
        this.medium = data.getString("medium")
        this.thumbnail = data.getString("thumbnail")

    }
}