package com.reza.lazuardy.testjsc.base
import androidx.recyclerview.widget.RecyclerView
import android.view.View

abstract class BaseViewHolder<in T>(itemView: View, private val onClickListener: BaseAdapter.OnItemClickListener) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView), View.OnClickListener {

    abstract fun bind(data: T)

    override fun onClick(v: View) = onClickListener.onItemClick(v, adapterPosition)
}