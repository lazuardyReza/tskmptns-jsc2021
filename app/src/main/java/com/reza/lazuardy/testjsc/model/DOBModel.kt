package com.reza.lazuardy.testjsc.model

import kotlinx.android.parcel.Parcelize
import android.os.Parcelable
import eu.amirs.JSON
import id.damcorp.baseapp.ekstensions.getKey
import id.damcorp.baseapp.ekstensions.getList
import id.damcorp.baseapp.ekstensions.getString


@Parcelize
data class DOBModel (
    var date:String="",
    var age:String=""
) : Parcelable {
    constructor(data: JSON) : this() {
        this.date = data.getString("date")
        this.age = data.getString("age")

    }
}