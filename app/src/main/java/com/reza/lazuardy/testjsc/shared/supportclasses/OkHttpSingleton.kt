package com.reza.lazuardy.testjsc.shared.supportclasses

import android.content.Context
import okhttp3.Interceptor
import java.util.concurrent.TimeUnit

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.security.KeyStore
import java.util.*
import javax.net.ssl.*

object OkHttpSingleton {
    lateinit var mInstance: OkHttpClient

    private fun getInterceptor(): Interceptor {
        return Interceptor { chain ->
            val request = chain.request()
            val builder = request.newBuilder()

            builder.addHeader("Content-Type", "application/json")
            chain.proceed(request)
        }
    }

    private val httpLoggingInterceptor = HttpLoggingInterceptor()
            .setLevel(HttpLoggingInterceptor.Level.BODY)

    fun getInstance(context: Context, sslSocketFactory: SSLSocketFactory?
                    , connectTimeout: Long = 30
                    , readTimeout: Long = 30
                    , writeTimeout: Long = 30): OkHttpClient {

//        var cache: Cache? = null
//        cache = Cache(cacheFile, (10 * 1024 * 1024).toLong())
        mInstance = OkHttpClient()
        if (sslSocketFactory != null) {
            mInstance = mInstance.newBuilder()
                    .addInterceptor(getInterceptor())
                    .addInterceptor(httpLoggingInterceptor)
                    .connectTimeout(connectTimeout, TimeUnit.SECONDS)
                    .readTimeout(readTimeout, TimeUnit.SECONDS)
                    .writeTimeout(writeTimeout, TimeUnit.SECONDS)
                    .sslSocketFactory(sslSocketFactory, trustManager())
                    .build()
        } else {
            mInstance = mInstance.newBuilder()
                    .addInterceptor(getInterceptor())
                    .addInterceptor(httpLoggingInterceptor)
                    .connectTimeout(connectTimeout, TimeUnit.SECONDS)
                    .readTimeout(readTimeout, TimeUnit.SECONDS)
                    .writeTimeout(writeTimeout, TimeUnit.SECONDS)
                    .build()
        }

        return mInstance
    }

    private fun trustManager(): X509TrustManager {
        val trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm())
        trustManagerFactory.init(null as KeyStore?)
        val trustManagers = trustManagerFactory.trustManagers
        if (trustManagers.size != 1 || trustManagers[0] !is X509TrustManager) {
            throw IllegalStateException("Unexpected default trust managers:" + Arrays.toString(trustManagers))
        }
        val trustManager = trustManagers[0] as X509TrustManager
        val sslContext = SSLContext.getInstance("SSL")
        sslContext.init(null, arrayOf<TrustManager>(trustManager), null)
        return trustManager
    }
}
