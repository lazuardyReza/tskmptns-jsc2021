package com.reza.lazuardy.testjsc.model

import kotlinx.android.parcel.Parcelize
import android.os.Parcelable
import eu.amirs.JSON
import id.damcorp.baseapp.ekstensions.getKey
import id.damcorp.baseapp.ekstensions.getList
import id.damcorp.baseapp.ekstensions.getString


@Parcelize
data class IdModel (
    var value:String="",
    var name:String="",
) : Parcelable {
    constructor(data: JSON) : this() {
        this.name = data.getString("name")
        this.value = data.getString("value")

    }
}