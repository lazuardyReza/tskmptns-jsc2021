package com.reza.lazuardy.testjsc.model.networking

import android.content.Context
import eu.amirs.JSON
import com.reza.lazuardy.testjsc.R
import com.reza.lazuardy.testjsc.model.StatusResponse
import com.reza.lazuardy.testjsc.shared.supportclasses.AppLog
import com.reza.lazuardy.testjsc.shared.supportclasses.OkHttpSingleton
import org.json.JSONObject
import java.io.IOException
import okhttp3.*
import okhttp3.RequestBody
import kotlin.jvm.Throws

class APIClient(val context: Context) {

    private val TAG = APIClient::class.java.simpleName

    val GET = "get"
    val POST = "post"
    val PATCH = "patch"

    companion object {
        val contectType = MediaType.parse("application/json; charset=utf-8")
        val contectTypeText = MediaType.parse("application/x-www-form-urlencoded")
    }

    private var okHttpClient: OkHttpClient = OkHttpSingleton.getInstance(context
            ,  null
            , connectTimeout = 120
            , readTimeout = 120
            , writeTimeout = 120)


    private var call: Call? = null

    fun callRestAPI(url: String, jObjParameters: JSONObject?, strMethod: String, delegateAPIRequest: DelegateAPIRequest) {
        val request = when(strMethod) {
            GET -> {
                Request.Builder()
                    .url(url)
                    .build()
            }
            POST -> {
                val body = RequestBody.create(contectType, jObjParameters.toString())
                Request.Builder()
                    .url(url)
                    .post(body)
                    .build()
            }
            PATCH -> {
                Request.Builder()
                    .url(url)
                    .patch(jObjParameters.toString().body())
                    .build()
            }
            else -> {
                Request.Builder()
                    .url(url)
                    .delete()
                    .build()
            }
        }

        call = okHttpClient.newCall(request)
        call?.enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                try {
                    if (jObjParameters != null) {
                        AppLog.d("onServiceCall-onFailure", "URL : $url, Params : $jObjParameters, call.isCanceled : ${call.isCanceled},error : $e")
                    } else {
                        AppLog.d("onServiceCall-onFailure", "URL : $url")
                    }
                } catch (e1: Exception) {
                    e1.printStackTrace()
                }
                delegateAPIRequest.onCallFailed(JSON("{}"),
                    StatusResponse(
                        false,
                        context.resources.getString(R.string.error_message_failed_get_response),
                        isCanceled = call.isCanceled
                    )
                )
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                AppLog.d("onServiceCall-onResponse", "sukses")
                val strResponse: String = response.body()?.string() ?: "{ \"status\":\"ERROR\", \"description\":\"ERROR\"}"
                if (response.isSuccessful && response.code() == 200) {
                    AppLog.d("onServiceCall-onResponse", "onCallSuccess")
                    delegateAPIRequest.onCallSuccess(JSON(strResponse))
                    response.body()?.close()
                } else {
                    AppLog.d("onServiceCall-onResponse", "onCallFailed")
                    delegateAPIRequest.onCallFailed(JSON(strResponse),
                        StatusResponse(
                            false,
                            response.code().toString()
                        )
                    )
                }
            }
        })
    }

    private fun String.body(): RequestBody {
        return RequestBody.create(contectType, this)
    }

    fun cancelAPIRequestCall() {
        if (call != null) {
            call?.cancel()
        }
    }
}
