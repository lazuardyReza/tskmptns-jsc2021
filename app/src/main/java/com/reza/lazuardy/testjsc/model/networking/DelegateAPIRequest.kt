package com.reza.lazuardy.testjsc.model.networking

import com.reza.lazuardy.testjsc.model.StatusResponse
import eu.amirs.JSON

interface DelegateAPIRequest {
    fun onCallSuccess(response: JSON)
    fun onCallFailed(response: JSON, statusResponse: StatusResponse)
}
