package com.reza.lazuardy.testjsc.base

interface BasePresenter<in T : BaseView> {

    fun onAttach(view: T)

    fun onDetach()
}