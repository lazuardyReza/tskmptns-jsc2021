package com.reza.lazuardy.testjsc.screen.detail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class DetailActivityView : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_view)
    }
}