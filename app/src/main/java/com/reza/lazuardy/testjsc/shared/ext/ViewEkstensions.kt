package id.damcorp.baseapp.ekstensions

import android.app.Activity
import android.content.Context
import android.graphics.Rect
import android.os.Build
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.Transformation
import android.view.inputmethod.InputMethodManager
import android.widget.EditText


/**
 * Created by ramadhan on 2/27/18.
 */

/**
 * set visible view
 */
fun View.toVisible() {
    visibility = View.VISIBLE
}

/**
 * set gone
 */
fun View.toGone() {
    visibility = View.GONE
}

/**
 * check visible
 */
fun View.isVisible() : Boolean {
    return visibility == View.VISIBLE
}

/**
 * set visible view
 */
fun View.toEnable() {
    isEnabled = true
}

/**
 * set gone
 */
fun View.toDisable() {
    isEnabled = false
}

fun View.setVisible(isVisible: Boolean) {
    if (isVisible) {
        visibility = View.VISIBLE
    } else {
        visibility = View.GONE
    }
}

fun Activity.showSoftKeyboard(editText: EditText) {
    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
}

fun Activity.hideSoftKeyboard() {
    val view = this.currentFocus
    if (view != null) {
        val imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}

fun Context.getStatusBarHeight(): Int {
    var result = 0
    val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
    if (resourceId > 0) {
        result = resources.getDimensionPixelSize(resourceId)
    }
    return result
}

fun View.setMarginTop(marginTop: Int) {
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) return
    val layoutParams = this.layoutParams as ConstraintLayout.LayoutParams
    layoutParams.setMargins(layoutParams.leftMargin, layoutParams.topMargin + marginTop,
            layoutParams.rightMargin, layoutParams.bottomMargin)
    this.layoutParams = layoutParams
}

fun Activity.getHeightKeyboard(): Int {
    val r = Rect()
    val activityRoot = getActivityRoot(this)
    activityRoot.getWindowVisibleDisplayFrame(r)
    return activityRoot.rootView.height - r.height()
}

private fun getActivityRoot(activity: Activity): View {
    return (activity.findViewById<View>(android.R.id.content) as ViewGroup).getChildAt(0)
}

fun expand(v: View) {
    v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    val targetHeight = v.measuredHeight
    // Older versions of android (pre API 21) cancel animations for views with a height of 0.
    v.layoutParams.height = 1
    v.visibility = View.VISIBLE
    val a = object : Animation() {
        override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
            v.layoutParams.height = if (interpolatedTime == 1f)
                ViewGroup.LayoutParams.WRAP_CONTENT
            else
                (targetHeight * interpolatedTime).toInt()
            v.requestLayout()
        }
        override fun willChangeBounds(): Boolean = true
    }
    // 1dp/ms
    a.duration = ((targetHeight / v.context.resources.displayMetrics.density).toInt() * 3).toLong()
    v.startAnimation(a)
}

fun collapse(v: View) {
    val initialHeight = v.measuredHeight
    val a = object : Animation() {
        override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
            if (interpolatedTime == 1f) {
                v.visibility = View.GONE
            } else {
                v.layoutParams.height = initialHeight - (initialHeight * interpolatedTime).toInt()
                v.requestLayout()
            }
        }
        override fun willChangeBounds(): Boolean = true
    }
    // 1dp/ms
    a.duration = ((initialHeight / v.context.resources.displayMetrics.density).toInt() * 3).toLong()
    v.startAnimation(a)
}