package id.damcorp.baseapp.ekstensions

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class Images : AppGlideModule()