package com.reza.lazuardy.testjsc.shared.ui

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.location.LocationManager
import android.os.Bundle
import android.view.Window
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import com.reza.lazuardy.testjsc.R
import com.reza.lazuardy.testjsc.base.BaseActivity
import id.damcorp.baseapp.ekstensions.getStatusBarHeight
import id.damcorp.baseapp.ekstensions.setMarginTop

open class AppActivity : BaseActivity() {
    private val TAG = AppActivity::class.java.simpleName

    val REQUEST_CHECK_SETTINGS = 1000
    private var dialogProgress: Dialog? = null
    private var dialogForceUpdate: Dialog? = null

    lateinit var locationManager: LocationManager



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initProgress()
    }

    override fun onResume() {
        super.onResume()
    }


    private fun initProgress() {
        // custom dialog
        dialogProgress = Dialog(this)
        dialogProgress?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogProgress?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialogProgress?.setContentView(R.layout.dialog_progress)
        dialogProgress?.setCancelable(false)
    }

    fun showProgress() { if (!isFinishing && dialogProgress?.isShowing == false) dialogProgress?.show() }
    fun hideProgress() { if (!isFinishing && dialogProgress?.isShowing == true) dialogProgress?.dismiss() }
    fun isProgress(): Boolean { return dialogProgress?.isShowing ?: false }
}