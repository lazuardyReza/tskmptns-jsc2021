package com.reza.lazuardy.testjsc.screen

import android.content.Context
import android.graphics.Typeface
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.reza.lazuardy.testjsc.R
import com.reza.lazuardy.testjsc.model.DataContact
import id.damcorp.baseapp.ekstensions.loadImage
import kotlinx.android.synthetic.main.item_contact.view.*
import java.util.*

class ContactAdapter(
    var data: ArrayList<DataContact>,
    private val listener: ContactListener
) : RecyclerView.Adapter<ContactAdapter.ContactViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {
        return ContactViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_contact, parent, false)
        )
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) =
        holder.bind(data[position], listener)

    fun swapData(data: ArrayList<DataContact>) {
        this.data = data
        notifyDataSetChanged()
    }

    inner class ContactViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: DataContact, listener: ContactListener) = with(itemView) {
            loadImage(
                itemView.context,
                item.pictureModel.thumbnail,
                itemView.ivProfile,
                R.drawable.ic_close
            )

            itemView.cardContact.setOnClickListener {
                listener.onClick(item)
            }

        }
    }

        interface ContactListener {
            fun onClick(item: DataContact)
        }

}