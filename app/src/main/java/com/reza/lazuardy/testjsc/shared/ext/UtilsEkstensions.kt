@file:JvmName("BaseUtils")

package id.damcorp.baseapp.ekstensions

import android.app.ActivityManager
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Context.CLIPBOARD_SERVICE
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.os.Environment
import android.os.Vibrator
import androidx.core.content.FileProvider
import android.util.DisplayMetrics
import android.view.View
import android.widget.ScrollView
import com.scottyab.rootbeer.RootBeer
import id.damcorp.baseapp.supportclasses.AppLog
import id.damcorp.rootvalidator.RootValidate
import io.michaelrocks.libphonenumber.android.PhoneNumberUtil
import io.michaelrocks.libphonenumber.android.Phonenumber
import io.michaelrocks.libphonenumber.android.NumberParseException
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.net.MalformedURLException
import java.net.URL
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

/**
 * Created by ramadhan on 3/2/18.
 */

const val DEFAULT_DATE = "dd MMMM yyyy"

inline fun <reified T : Any> clazz() = T::class.java

fun convertDpToPx(dp: Int): Int {
    val metrics = Resources.getSystem().displayMetrics
    return Math.round(dp * (metrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))
}

fun String.isValidEmail(): Boolean {
    val EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"

    val pattern = Pattern.compile(EMAIL_PATTERN)
    val matcher = pattern.matcher(this)
    return matcher.matches()
}

fun Context.validPhoneNumber(number: String, region: String): Boolean {
    val util = PhoneNumberUtil.createInstance(this)
    var phoneNumber: Phonenumber.PhoneNumber? = null
    try {
        phoneNumber = util.parse(number, region)
    } catch (e: NumberParseException) {
        System.err.println("NumberParseException was thrown: " + e.toString())
        return false
    }

    return util.isValidNumber(phoneNumber)
}

fun Context.vibrate(long: Long) {
    val v = this.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
    v.vibrate(300)
}

//this method to set string using capitalize format
fun String.toCapitalize(): String {
    val words = this.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
    val sb = StringBuilder()
    if (words[0].isNotEmpty()) {
        sb.append(Character.toUpperCase(words[0][0]) + words[0].subSequence(1, words[0].length).toString().toLowerCase())
        for (j in 1 until words.size) {
            sb.append(" ")
            sb.append(Character.toUpperCase(words[j][0]) + words[j].subSequence(1, words[j].length).toString().toLowerCase())
        }
    }

    return sb.toString()
}

fun Double.currencyFormater(): String {
    val myFormatter = DecimalFormat("###,###,###")
    return myFormatter.format(this).replace(",", ".")
}

fun loadBitmapFromView(v: View, width: Int, height: Int): Bitmap {
    val b = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
    val c = Canvas(b)
    v.draw(c)
    return b
}

fun saveScreenshotToDevice(v: View): File {
    val bm = loadBitmapFromView(v, v.width, v.height)

    val root = Environment.getExternalStorageDirectory().toString()
    val myDir = File("$root/req_images")
    myDir.mkdirs()
    val fname = "struct.jpg"
    val file = File(myDir, fname)
    AppLog.i("dirfile", "" + file)
    if (file.exists())
        file.delete()
    return try {
        val out = FileOutputStream(file)
        bm.compress(Bitmap.CompressFormat.JPEG, 90, out)
        out.flush()
        out.close()
        file
    } catch (e: Exception) {
        e.printStackTrace()
        file
    }
}

fun Context.shareImage(file: File, title: String) {
    val share = Intent(Intent.ACTION_SEND)
    share.type = "image/*"
    val photoURI = FileProvider.getUriForFile(this, this.applicationContext.packageName + ".provider", file)
    share.putExtra(Intent.EXTRA_STREAM, photoURI)
    share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
    this.startActivity(Intent.createChooser(share, title))
}

fun String.codeName(): String {
    var name = this
    if (name.startsWith(" ")) {
        name = name.removePrefix(" ")
    }
    if (name.endsWith(" ")) {
        name = name.removeSuffix(" ")
    }
    val arrName = name.split(" ")
    var initialName = ""
    if (arrName.isNotEmpty()) {
        initialName = when (arrName.size) {
            1 -> arrName[0].substring(0, 1).toUpperCase()
            else -> {
                "${arrName[0].substring(0, 1).toUpperCase()}${arrName[arrName.size.minus(1)].substring(0, 1).toUpperCase()}"
            }
        }
    }
    return initialName
}

fun saveScreenshotScrollToDevice(scrollView: ScrollView): File {
    val totalHeight = scrollView.getChildAt(0).height
    val totalWidth = scrollView.getChildAt(0).width

    val bitmap = getBitmapFromView(scrollView, totalHeight, totalWidth)

    //Save bitmap
    val root = Environment.getExternalStorageDirectory().toString()
    val myDir = File("$root/req_images")
    myDir.mkdirs()
    val fname = "struct.jpg"
    val file = File(myDir, fname)
    AppLog.i("dirfile", "" + file)
    if (file.exists())
        file.delete()
    var fos: FileOutputStream? = null
    return try {
        fos = FileOutputStream(file)
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos)
        fos.flush()
        fos.close()
//        MediaStore.Images.Media.insertImage(mContext.getContentResolver(), bitmap, "Screen", "screen")
        file
    } catch (e: FileNotFoundException) {
        // TODO Auto-generated catch block
        e.printStackTrace()
        file
    } catch (e: Exception) {
        // TODO Auto-generated catch block
        e.printStackTrace()
        file
    }

}

private fun getBitmapFromView(view: View, totalHeight: Int, totalWidth: Int): Bitmap {
    val returnedBitmap = Bitmap.createBitmap(totalWidth, totalHeight, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(returnedBitmap)
    val bgDrawable = view.background
    if (bgDrawable != null)
        bgDrawable.draw(canvas)
    else
        canvas.drawColor(Color.WHITE)
    view.draw(canvas)
    return returnedBitmap
}

fun dateFormat(date: String, oldFormat: String, newFormat: String): String {
    val dateFormatBefore = SimpleDateFormat(oldFormat, Locale.getDefault())
    val dateNew: Date = dateFormatBefore.parse(date)
    val dateFormat = SimpleDateFormat(newFormat, Locale.getDefault())
    return dateFormat.format(dateNew)
}

fun Context.copyToClipboard(value: String) {
    val clipboard = getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
    val clip = ClipData.newPlainText("label", value)
    clipboard.primaryClip = clip
}

fun Context.getRunningActivity(): String {
    val activityManager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
    val componentName = activityManager.getRunningTasks(1)[0].topActivity
    return componentName.className
}

fun String.validateInput(): String {
    var input = this
    if (input.startsWith(" ")) input = input.substring(1)
    if (input.endsWith(" ")) input = input.substring(0, input.length.minus(2))
    return input
}

/**
 * Check if the device's camera has a Flashlight.
 *
 * @return true if there is Flashlight, otherwise false.
 */
fun Context.hasFlash(): Boolean {
    return applicationContext.packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)
}

fun Context.isRooted(): Boolean {
    /*val rootBeer = RootBeer(this)
    rootBeer.isRootedWithoutBusyBoxCheck
    return rootBeer.isRooted*/
    return RootValidate().run()
}

fun getBaseUrl(stringUrl: String): String {
    return try {
        val url = URL(stringUrl)
        val baseUrl = url.protocol + "://" + url.host
        baseUrl
    } catch (e: MalformedURLException) {
        // do something
        ""
    }
}