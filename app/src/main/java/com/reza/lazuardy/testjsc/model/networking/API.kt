package com.reza.lazuardy.testjsc.model.networking

import android.content.Context
import com.google.gson.Gson
import com.reza.lazuardy.testjsc.model.DataContact
import com.reza.lazuardy.testjsc.model.StatusResponse
import eu.amirs.JSON
import id.damcorp.baseapp.ekstensions.getList
import org.json.JSONArray
import org.json.JSONObject

class API(val context: Context)
{

        private val TAG = API::class.java.simpleName

        private val apiClient: APIClient = APIClient(context)

        private val BASE_URL = "https://randomuser.me/api?results=5&exc=login,registered,i d,nat&nat=us&noinfo"

    fun getContact(callback: (ArrayList<DataContact>, StatusResponse) -> Unit) {
        apiClient.callRestAPI("$BASE_URL",
            JSONObject(), apiClient.GET, object : DelegateAPIRequest {
                override fun onCallSuccess(response: JSON) {
                    var arrDataContact: ArrayList<DataContact> = ArrayList()
                    for (_data in response.getList("results")) {
                        arrDataContact.add(DataContact(_data))
                    }
                    callback(arrDataContact, StatusResponse(true, "", ""))
                }

                override fun onCallFailed(response: JSON, statusResponse: StatusResponse) {
                    callback(ArrayList<DataContact>(), StatusResponse(false, "description", ""))
                }


            })
    }




}