package com.reza.lazuardy.testjsc.shared.ext

import androidx.annotation.StringDef
import kotlin.annotation.AnnotationRetention.SOURCE

@Retention(SOURCE)
@StringDef(

)

annotation class Configs

object Config {

}