package com.reza.lazuardy.testjsc.model

import kotlinx.android.parcel.Parcelize
import android.os.Parcelable
import eu.amirs.JSON
import id.damcorp.baseapp.ekstensions.getKey
import id.damcorp.baseapp.ekstensions.getList
import id.damcorp.baseapp.ekstensions.getString
import java.sql.Time


@Parcelize
data class LocationModel (
    var streetModel: StreetModel= StreetModel(),
    var city:String="",
    var state:String="",
    var country:String="",
    var postcode:String="",
    var coordinateModel: CoordinateModel=CoordinateModel(),
    var timezoneModel: TimezoneModel=TimezoneModel(),
) : Parcelable {
    constructor(data: JSON) : this() {
        this.streetModel = StreetModel(data.getKey("street"))
        this.city = data.getString("city")
        this.state = data.getString("state")
        this.country = data.getString("country")
        this.postcode = data.getString("postcode")
        this.coordinateModel = CoordinateModel(data.getKey("coordinates"))
        this.timezoneModel = TimezoneModel(data.getKey("timezone"))
    }
}