@file:JvmName("BaseSecurity")
package id.damcorp.baseapp.ekstensions

import java.text.SimpleDateFormat
import java.util.*

fun generateTraceNumber(): String {
    val currentTime = Calendar.getInstance().time
    val simpleDateFormat = SimpleDateFormat("yyMMDDHHmmssSSSS", Locale.US)
    val rand = Random()
    val randomNumber = 100000 + rand.nextInt(900000)
    return "$randomNumber${simpleDateFormat.format(currentTime)}"
}
