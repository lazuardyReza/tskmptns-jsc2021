package com.reza.lazuardy.testjsc.base

import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import android.view.WindowManager
import android.widget.TextView
import androidx.multidex.BuildConfig
import com.reza.lazuardy.testjsc.R
import id.damcorp.baseapp.ekstensions.getStatusBarHeight
import id.damcorp.baseapp.ekstensions.setMarginTop

/**
 * Created by ramadhan on 2/21/18.
 */
open class BaseActivity: AppCompatActivity() {

    var isEnterFromRight = true

    override fun onCreate(savedInstanceState: Bundle?) {
        if (isEnterFromRight) overridePendingTransition(R.anim.enter_from_right, R.anim.no_anim)
        super.onCreate(savedInstanceState)
        if (!BuildConfig.DEBUG) window.setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }


    fun initToolbar(toolbar: Toolbar, title: String, iconBack: Drawable) {
        setSupportActionBar(toolbar)
        supportActionBar?.title = title
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(false)
        toolbar.navigationIcon = iconBack
        toolbar.setNavigationOnClickListener { onBackPressed() }
        toolbar.setMarginTop(getStatusBarHeight())
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean("is_enter_from_right", isEnterFromRight)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        isEnterFromRight = savedInstanceState.getBoolean("is_enter_from_right")
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (isEnterFromRight) overridePendingTransition(R.anim.no_anim, R.anim.exit_to_right)
    }

}