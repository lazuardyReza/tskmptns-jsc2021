package com.reza.lazuardy.testjsc.shared.supportclasses

import android.util.Log
import androidx.multidex.BuildConfig

/**
 * Created by ramadhan on 2/21/18.
 */
object AppLog {
    var DEBUG = BuildConfig.DEBUG//Log.isLoggable(TAG, Log.VERBOSE);

    fun v(tag: String, msg: String) {
        if (DEBUG) {
            Log.v(tag, msg)
        }
    }

    fun d(tag: String, msg: String) {
        if (DEBUG) {
            Log.d(tag, msg)
        }
    }

    fun i(tag: String, msg: String) {
        if (DEBUG) {
            Log.i(tag, msg)
        }
    }

    fun e(tag: String, msg: String) {
        if (DEBUG) {
            Log.e(tag, msg)
        }
    }

    fun w(tag: String, msg: String) {
        if (DEBUG) {
            Log.w(tag, msg)
        }
    }
}
