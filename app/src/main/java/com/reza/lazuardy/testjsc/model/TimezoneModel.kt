package com.reza.lazuardy.testjsc.model

import kotlinx.android.parcel.Parcelize
import android.os.Parcelable
import eu.amirs.JSON
import id.damcorp.baseapp.ekstensions.getKey
import id.damcorp.baseapp.ekstensions.getList
import id.damcorp.baseapp.ekstensions.getString


@Parcelize
data class TimezoneModel (
    var offset:String="",
    var description:String=""
) : Parcelable {
    constructor(data: JSON) : this() {
        this.offset = data.getString("offset")
        this.description = data.getString("description")
    }
}