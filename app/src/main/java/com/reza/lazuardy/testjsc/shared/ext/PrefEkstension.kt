package id.damcorp.baseapp.ekstensions

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import id.damcorp.baseapp.BaseConstants
import com.scottyab.aescrypt.AESCrypt
import java.security.GeneralSecurityException


/**
 * Basic Pref
 */
fun Context.pref(): SharedPreferences {
    return this.getSharedPreferences(BaseConstants.PREF.NAME, Activity.MODE_PRIVATE)
}

/**
 * Checking pref
 */
fun Context.prefContains(strKey: String): Boolean {
    return this.pref().contains(strKey)
}

/**
 * Encrypt Pref
 */
private fun encryptPref(value: String): String {
    val encrypted: String
    try {
        encrypted = AESCrypt.encrypt(BaseConstants.PREF.PASSWORD_PREF, value)
    } catch (e: GeneralSecurityException) {
        //handle error
        return ""
    }
    return encrypted
}

/**
 * Decrypt Pref
 */
private fun decryptPref(value: String): String {
    val decrypted: String
    try {
        decrypted = AESCrypt.decrypt(BaseConstants.PREF.PASSWORD_PREF, value)
    } catch (e: GeneralSecurityException) {
        //handle error
        return ""
    }
    return decrypted
}

/**
 * save to pref - contex
 */
fun Context.saveToPref(strKey: String, value: Any?) {
    /* checking value type */
    val editor = this.pref().edit()
    when (value) {
        is String   -> editor.putString(strKey, encryptPref(value))
        is Boolean  -> editor.putBoolean(strKey, value)
        is Float    -> editor.putFloat(strKey, value)
        is Int      -> editor.putInt(strKey, value)
        is Long     -> editor.putLong(strKey, value)
    }
    /* save pref */
    editor.apply()
}

/**
 * delete from pref - Context
 */
fun Context.deleteFromPref(strKey: String) {
    val editor = this.pref().edit()
    editor.remove(strKey)
    /* save pref */
    editor.apply()
}

/**
 * get value from pref
 */
fun Context.getStringFromPref(strKey: String): String {
    return if (prefContains(strKey)) {
        val value = this.pref().getString(strKey, "")
        var valueDecrypt = decryptPref(value)
        if (valueDecrypt.isNullOrBlank()) {
            valueDecrypt = ""
        }
        valueDecrypt
    } else {
        ""
    }
}
fun Context.getBooleanFromPref(strKey: String): Boolean {
    return this.pref().getBoolean(strKey, false)
}
fun Context.getIntFromPref(strKey: String): Int {
    return this.pref().getInt(strKey, 0)
}
fun Context.getLongFromPref(strKey: String): Long {
    return this.pref().getLong(strKey, 0)
}

/* custom pref */
fun Context.setPrefUser(user: String) {
    val editor = this.pref().edit()
    editor.putString(BaseConstants.PREF.USER, encryptPref(user))
    /* save pref */
    editor.apply()
}
fun Context.getPrefUser(): String {
    return if (this.prefContains(BaseConstants.PREF.USER)) {
        val value = this.pref().getString(BaseConstants.PREF.USER, "{}")
        var valueDecrypt = decryptPref(value)
        if (valueDecrypt.isNullOrBlank() || valueDecrypt.isEmpty()) {
            valueDecrypt = "{}"
        }
        valueDecrypt
    } else {
        "{}"
    }
}
fun Context.removePrefUser() {
    val editor = this.pref().edit()
    editor.remove(BaseConstants.PREF.USER)
    /* save pref */
    editor.apply()
}
/* Helper for product */
fun Context.setPrefProducts(products: String) {
    val editor = this.pref().edit()
    editor.putString(BaseConstants.PREF.PRODUCT_LIST, encryptPref(products))
    /* save pref */
    editor.apply()
}
fun Context.getPrefProducts(): String {
    return if (this.prefContains(BaseConstants.PREF.PRODUCT_LIST)) {
        val value = this.pref().getString(BaseConstants.PREF.PRODUCT_LIST, "{}")
        var valueDecrypt = decryptPref(value)
        if (valueDecrypt.isNullOrBlank() || valueDecrypt.isEmpty()) {
            valueDecrypt = "{}"
        }
        valueDecrypt
    } else {
        "{}"
    }
}
fun Context.removePrefProducts() {
    val editor = this.pref().edit()
    editor.remove(BaseConstants.PREF.PRODUCT_LIST)
    /* save pref */
    editor.apply()
}
/* Helper for Favourite */
fun Context.setPrefFavourite(favourites: String) {
    val editor = this.pref().edit()
    editor.putString(BaseConstants.PREF.FAVOURITE_LIST, encryptPref(favourites))
    /* save pref */
    editor.apply()
}
fun Context.getPrefFavourite(): String {
    return if (this.prefContains(BaseConstants.PREF.FAVOURITE_LIST)) {
        val value = this.pref().getString(BaseConstants.PREF.FAVOURITE_LIST, "{}")
        var valueDecrypt = decryptPref(value)
        if (valueDecrypt.isNullOrBlank() || valueDecrypt.isEmpty()) {
            valueDecrypt = "{}"
        }
        valueDecrypt
    } else {
        "{}"
    }
}
fun Context.removePrefFavourite() {
    val editor = this.pref().edit()
    editor.remove(BaseConstants.PREF.FAVOURITE_LIST)
    /* save pref */
    editor.apply()
}

fun Context.setPrefListPending(listPending: String) {
    val editor = this.pref().edit()
    editor.putString(BaseConstants.PREF.PENDING_LIST, encryptPref(listPending))
    /* save pref */
    editor.apply()
}
fun Context.getPrefListPending(): String {
    return if (this.prefContains(BaseConstants.PREF.PENDING_LIST)) {
        val value = this.pref().getString(BaseConstants.PREF.PENDING_LIST, "{}")
        var valueDecrypt = decryptPref(value)
        if (valueDecrypt.isNullOrBlank() || valueDecrypt.isEmpty()) {
            valueDecrypt = "{}"
        }
        valueDecrypt
    } else {
        "{}"
    }
}

/* custom pref */
fun Context.setPrefClientId(user: String) {
    val editor = this.pref().edit()
    editor.putString(BaseConstants.PREF.CLIENT_ID, encryptPref(user))
    /* save pref */
    editor.apply()
}
fun Context.getPrefClientId(): String {
    return if (this.prefContains(BaseConstants.PREF.CLIENT_ID)) {
        val value = this.pref().getString(BaseConstants.PREF.CLIENT_ID, "{}")
        var valueDecrypt = decryptPref(value)
        if (valueDecrypt.isNullOrBlank() || valueDecrypt.isEmpty()) {
            valueDecrypt = "{}"
        }
        valueDecrypt
    } else {
        "{}"
    }
}