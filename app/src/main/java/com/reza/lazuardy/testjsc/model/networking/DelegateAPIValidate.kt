package com.reza.lazuardy.testjsc.model.networking

interface DelegateAPIValidate {
    fun onValidate(isValidate: Boolean, description: String, responseCode: String = "")
}
