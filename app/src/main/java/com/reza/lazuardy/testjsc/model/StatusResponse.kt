package com.reza.lazuardy.testjsc.model

data class StatusResponse(var isSuccess: Boolean = true,
                          var errorCode: String = "",
                          var message: String = "",
                          var api: String = "",
                          var isCanceled: Boolean = false)