package com.reza.lazuardy.testjsc.base

interface BaseView {

    fun onAttach()

    fun onDetach()
}