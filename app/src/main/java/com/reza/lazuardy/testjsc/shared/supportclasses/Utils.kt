package com.reza.lazuardy.testjsc.shared.supportclasses

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Handler
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.ArrayAdapter
import android.widget.EditText
import androidx.constraintlayout.widget.ConstraintLayout
import id.damcorp.baseapp.ekstensions.convertDpToPx
import id.damcorp.baseapp.ekstensions.getHeightKeyboard
import id.damcorp.baseapp.ekstensions.getStatusBarHeight
import id.damcorp.baseapp.ekstensions.toCapitalize
import id.damcorp.digiapp.BuildConfig
import id.damcorp.digiapp.R
import id.damcorp.digiapp.model.*
import id.damcorp.digiapp.screen.auth.InputNumberView
import id.damcorp.digiapp.screen.auth.InputPinView
import id.damcorp.digiapp.shared.ext.Constants
import id.damcorp.digiapp.shared.preferences.getPosOrder
import id.damcorp.digiapp.shared.preferences.getUser
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern
import kotlin.collections.ArrayList


fun Context.getStringRes(int: Int): String {
    return resources.getString(int)
}

fun EditText.onDone(callback: () -> Unit) {
    setOnEditorActionListener { _, actionId, _ ->
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            callback.invoke()
            true
        }
        false
    }
}

fun isNumeric(strNum: String?): Boolean {
    if (strNum == null) {
        return false
    }
    try {
        val d = strNum.toDouble()
    } catch (nfe: NumberFormatException) {
        return false
    }
    return true
}

fun Context.getAddress(latitude: Double, longitude: Double): LocationAddress {
    val addresses: List<Address>
    val locationAddress = LocationAddress()
    try {
        val geocoder = Geocoder(this, Locale.getDefault())

        addresses = geocoder.getFromLocation(
            latitude,
            longitude,
            1
        ) // Here 1 represent max location result to returned, by documents it recommended 1 to 5

        locationAddress.latitude = latitude
        locationAddress.longitude = longitude
        locationAddress.alamat = addresses[0].getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        locationAddress.jalan = addresses[0].featureName // Only if available else return NULL
        locationAddress.desa = addresses[0].subLocality
        locationAddress.kecamatan = addresses[0].locality
        locationAddress.kabupaten = addresses[0].subAdminArea
        locationAddress.provinsi = addresses[0].adminArea
        locationAddress.negara = addresses[0].countryName
        locationAddress.kodePos = addresses[0].postalCode
    } catch (e: Exception) {
        AppLog.d("getAddress", e.toString())
    }

    return locationAddress
}

fun String.getStatusResponse(): String {
    val statusResponse = this.replace("_", " ")
    return if (statusResponse.isNotEmpty()) statusResponse.toCapitalize() else statusResponse
}

interface OnDelayListener {
    fun onFinishDelay()
}

fun postDelay(listener: OnDelayListener, delay: Long) {
    Handler().postDelayed({
        listener.onFinishDelay()
    }, delay)
}

fun getShownNominalNoneConvert(s: String): String {
    var s = s
    var result = ""
    val a = s.replace(" ", "")
    var j = ""
    var k = a
    if (a.endsWith(".000000")) {
        j = a.substring(0, a.length - 7)
        k = j
    }
    if (a.endsWith(".00")) {
        j = a.substring(0, a.length - 3)
        k = j
    }
    if (a.endsWith(".0")) {
        j = a.substring(0, a.length - 2)
        k = j
    }
    val z = k.replace("-", "")
    val b = z.replace("Rp.", "")
    val c = b.replace("Rp", "")
    val d = c.replace(",00", "")
    s = d.replace(".", "")
    var count = 1
    for (i in s.length - 1 downTo 0) {
        result = s[i].toString() + result
        if (count % 3 == 0 && i != 0) result = ".$result"
        count++
    }
    return "Rp $result"
}

fun getNormalName(name: String): String{
    var nameToShow: String = name
    var param = ""
    if (name.contains("]")) {
        // get name
        val intBracket: Int = name.indexOf("]") + 1
        nameToShow = name.substring(intBracket)
        if (nameToShow.startsWith(" ")) nameToShow = nameToShow.substring(1)
        // get param
        val p =
            Pattern.compile("\\[(.*?)\\]") // the pattern to search for
        val m = p.matcher(name)
        // if we find a match, get the group
        if (m.find()) {
            // we're only looking for one group, so get it
            param = m.group(1)
            // print the group out for verification
        }
    }

    return nameToShow
}

fun getNormalizeNumber(s: String): String {
    var s = s
    var result = ""
    val a = s.replace(" ", "")
    var j = ""
    var k = a
    if (a.endsWith(".000000")) {
        j = a.substring(0, a.length - 7)
        k = j
    }
    if (a.endsWith(".00")) {
        j = a.substring(0, a.length - 3)
        k = j
    }
    if (a.endsWith(".0")) {
        j = a.substring(0, a.length - 2)
        k = j
    }
    val z = k.replace(".", "")
    val b = z.replace("Rp", "")

    s = b.replace(" ", "")
    result = s
    return "" + result
}

fun getDateNow(format: String, isGMT0: Boolean = false): String {
    val calendar = Calendar.getInstance().time
    val output = SimpleDateFormat(format, Locale.getDefault())
    if (isGMT0) output.timeZone = TimeZone.getTimeZone("GBR")
    return output.format(calendar)
}

fun isAfterDateTime(strDate1: String, strDate2: String, dateFormat: String, isGMT0: Boolean = false): Boolean {
    val serviceDateFormat = SimpleDateFormat(dateFormat, Locale.getDefault())
    val date1 = serviceDateFormat.parse(strDate1)
    val date2 = serviceDateFormat.parse(strDate2)
    val fmt = SimpleDateFormat("yyyyMMdd HH:mm:dd", Locale.getDefault())
    if (isGMT0) fmt.timeZone = TimeZone.getTimeZone("GBR")
    return fmt.format(date1) < fmt.format(date2)
}

fun isBeforeDateTime(
    strDate1: String,
    strDate2: String,
    dateFormat: String,
    isGMT0: Boolean = false
): Boolean {
    val serviceDateFormat = SimpleDateFormat(dateFormat, Locale.getDefault())
    val date1 = serviceDateFormat.parse(strDate1)
    val date2 = serviceDateFormat.parse(strDate2)
    val fmt = SimpleDateFormat("yyyyMMdd HH:mm:dd", Locale.getDefault())
    if (isGMT0) fmt.timeZone = TimeZone.getTimeZone("GBR")
    return fmt.format(date1) > fmt.format(date2)
}

fun String.getSalesCode(): String {
    return when(this) {
        Constants.VALUE.DINE_IN -> Constants.VALUE.SALES_DINE_IN
        Constants.VALUE.TAKE_AWAY -> Constants.VALUE.SALES_TAKE_AWAY
        else -> Constants.VALUE.SALES_DELIVERY
    }
}

fun String.getSalesCodeForNote(): String {
    return when(this) {
        Constants.VALUE.DINE_IN -> Constants.VALUE.SALES_DINE_IN
        Constants.VALUE.TAKE_AWAY -> Constants.VALUE.SALES_TAKE_AWAY
        else -> Constants.VALUE.SALES_DELIVERY_
    }
}

fun String.getTaxServCode(): String {
    return when(this) {
        Constants.VALUE.DINE_IN -> Constants.VALUE.TAX_SERV_DINE_IN
        Constants.VALUE.TAKE_AWAY -> Constants.VALUE.TAX_SERV_TAKE_AWAY
        Constants.VALUE.ONLINE_ORDER -> Constants.VALUE.TAX_SERV_DELIVERY
        else -> Constants.VALUE.TAX_SERV_DRIVE_THRU
    }
}

// validate product or modifier
fun isValid(
    price: String,
    category: String,
    isSellable: Boolean,
    stock: String,
    isUseSalesType: Boolean,
    listSalesTypes: ArrayList<POSSalesType>,
    orderMethod: String
): Boolean {
    // check category
    if (category.equals("HIDDEN", true)) return false
    // check stock
    if (stock.isValidInt())
        if (stock.toInt() == 0) return false
    // check isSellable
    if (!isSellable) return false
    // check isUseSalesType
    if (isUseSalesType) {
        var isContainType: Boolean = false
        for (salesType in listSalesTypes) {
            if (salesType.code == orderMethod.getSalesCode()) {
                isContainType = when {
                    salesType.price.isEmpty()|| !isNumeric(salesType.price) -> false
                    else -> true
                }
                break
            }
        }
        if (!isContainType) return false
    } else {
        if (price.isEmpty() || !isNumeric(price)) return false
    }
    return true
}

fun String.validInt(): Int {
    return if (this.isNotEmpty() && isNumeric(this)) this.toInt() else 0
}

fun String.isValidInt(): Boolean {
    return this.isNotEmpty() && isNumeric(this)
}

fun POSPromo.isValidPromo(): Boolean {
    val dateTimeFormat = "yyyy-MM-dd HH:mm:ss"
    val dateTimeNow = getDateNow(dateTimeFormat)
    val dayNow = getDateNow("EEEE").toLowerCase()
    var isValid = true
    // check isActive
    if (!this.isActive) isValid = false
    // check startDateTime
    if (!isAfterDateTime(this.startDateTime, dateTimeNow, dateTimeFormat)) isValid = false
    AppLog.d("isValidPromo-isAfterDateTime", "${this.startDateTime} // $dateTimeNow // $isValid")
    // check endDateTime
    if (!isBeforeDateTime(this.endDateTime, dateTimeNow, dateTimeFormat)) isValid = false
    AppLog.d("isValidPromo-endDateTime", "${this.endDateTime} // $dateTimeNow // $isValid")
    // check day
    if (!this.validDay.contains(dayNow)) isValid = false
    AppLog.d("isValidPromo-contains", "$isValid")
    return isValid
}

fun Int.getTimeExpired(): String {
    var milliseconds = System.currentTimeMillis()
    milliseconds = milliseconds.plus(this * 1000)
    val seconds = (milliseconds / 1000) % 60
    val minutes = (milliseconds / (1000 * 60) % 60)
    val hours = (milliseconds / (1000 * 60 * 60) % 24)
    return String.format("%02d:%02d:%02d", hours, minutes, seconds)
}

fun String.setAppName(context: Context): String {
    return String.format(this, context.getStringRes(R.string.app_name))
}

fun formatDate(date: String, formatBefore: String, formatAfter: String): String{
    val dateFormatBefore = SimpleDateFormat(formatBefore, Locale.getDefault())
    val dateNew: Date = dateFormatBefore.parse(date)
    val tz = TimeZone.getTimeZone("Asia/Jakarta")
    val dateFormat = SimpleDateFormat(formatAfter, Locale.getDefault())
    dateFormat.timeZone = tz
    dateNew.time
    return dateFormat.format(dateNew.time + (7 * 60 * 60 * 1000))
}

fun Context.doCall(phone: String) {
    val uri = "tel:$phone"
    val intent = Intent(Intent.ACTION_DIAL)
    intent.data = Uri.parse(uri)
    startActivity(intent)
}

fun Context.doEmail(email: String) {
    val intent = Intent(
        Intent.ACTION_SENDTO, Uri.fromParts(
            "mailto", email, null
        )
    )
    intent.putExtra(Intent.EXTRA_SUBJECT, resources.getString(R.string.contact_email_subject))
    startActivity(
        Intent.createChooser(
            intent,
            resources.getString(R.string.contact_share_email_title)
        )
    )
}

fun Context.doBrowser(url: String) {
    val intent = Intent(Intent.ACTION_VIEW)
    intent.data = Uri.parse(url)
    startActivity(intent)
}

fun Context.getProductToAddToCart(posOutlet: POSOutlet): ArrayList<POSProduct> {
    val listProduct = ArrayList<POSProduct>()
    listProduct.addAll(getPosOrder().listProduct)
    if (listProduct.size == 0) return ArrayList()
    // validate sales type
    when(posOutlet.selectedOrderMethod) {
        Constants.VALUE.DINE_IN -> {
        }
        Constants.VALUE.TAKE_AWAY -> {
        }
        Constants.VALUE.ONLINE_ORDER -> {
        }
    }
    return listProduct
}

fun Activity.setBottomShowKeyboard(view: View, marginBottomView: Int) {
    KeyboardVisibilityEvent.setEventListener(this) {
        // some code depending on keyboard visiblity status
        val bottomMargin = if (it) {
            (getHeightKeyboard() - getStatusBarHeight()) + convertDpToPx(marginBottomView)
        } else {
            convertDpToPx(marginBottomView)
        }
        val layoutParamsButton = view.layoutParams as ConstraintLayout.LayoutParams
        layoutParamsButton.setMargins(
            layoutParamsButton.leftMargin, layoutParamsButton.topMargin,
            layoutParamsButton.rightMargin, bottomMargin
        )
        view.layoutParams = layoutParamsButton
    }
}


fun String.convertPhoneNumberID(): String {
    return when {
        this.startsWith("8") -> "0$this"
        this.startsWith("62") -> "0${this.substring(2, this.length)}"
        this.startsWith("+62") -> "0${this.substring(3, this.length)}"
        else -> this
    }
}

fun String.isPhoneNumber(): Boolean {
    if (this.startsWith("+")) {
        if (!this.startsWith("+62")) {
            return false
        }
    }

    var strPhoneNumber: String = this
    if (strPhoneNumber.startsWith("62")) {
        strPhoneNumber = "0${this.substring(2, this.length)}"
    } else if (strPhoneNumber.startsWith("+62")) {
        strPhoneNumber = "0${this.substring(3, this.length)}"
    }
    AppLog.d("convertPhoneNumberID-isPhoneNumber", strPhoneNumber)

//    val PHONE_REGEX =  "^(^0856|^0811|^0812|^0813|^0821|^0822|^0823|^0852|^0853|^0851|^0814|^0815|^0816|^0855|^0857|^0858|^0817|^0818|^0819|^0877|^0878|^0879|^0838|^0831|^0859|^0832|^0833)|^08098888"
    val PHONE_REGEX = "^(^08).*\$"
    val pattern = Pattern.compile(PHONE_REGEX)
    val matcher = pattern.matcher(strPhoneNumber)
    AppLog.d("convertPhoneNumberID-PHONE_REGEX", "${matcher.matches()}")

    return matcher.matches()
}

fun Activity.intentAuth(requestCode: Int) {
    startActivityForResult(
        when (getUser().phone.isEmpty()) {
            true -> InputNumberView.newIntent(this, false)
            false -> InputPinView.newIntent(this, getUser().phone)
        }, requestCode
    )
}

fun remoteCustomerService(): String {
    return when(BuildConfig.FLAVOR) {
        "maqan" -> Constants.REMOTE.CUSTOMER_SERVICE_MAQAN
        "digiresto" -> Constants.REMOTE.CUSTOMER_SERVICE
        else -> Constants.REMOTE.CUSTOMER_SERVICE
    }
}

fun aboutAppService(): String {
    return when(BuildConfig.FLAVOR) {
        "maqan" -> Constants.REMOTE.ABOUT_APP_MAQAN
        "digiresto" -> Constants.REMOTE.ABOUT_APP
        else -> Constants.REMOTE.ABOUT_APP
    }
}

fun getTimeHourNow(): Int {
    val calendar = Calendar.getInstance()
    return calendar[Calendar.HOUR_OF_DAY]
}

fun listTimeDineIn(): ArrayList<String> {
    val listTime = ArrayList<String>()
    var timeNow = getTimeHourNow()
    while (timeNow < 24) {
        timeNow += 1
        listTime.add("$timeNow:00")
    }
    AppLog.d("listTimeDineIn", listTime.toString())
    return listTime
}

fun Context.spinnerAdapter(listSpinner: java.util.ArrayList<String>): ArrayAdapter<String> {
    val adapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listSpinner)
    adapter.setDropDownViewResource(R.layout.item_spinner_dropdown)
    return adapter
}