package com.reza.lazuardy.testjsc.screen

import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.appbar.AppBarLayout
import com.reza.lazuardy.testjsc.R
import com.reza.lazuardy.testjsc.base.BaseActivity
import com.reza.lazuardy.testjsc.model.DataContact
import com.reza.lazuardy.testjsc.shared.supportclasses.getStringRes
import com.reza.lazuardy.testjsc.shared.ui.AppActivity
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppActivity() {

    private lateinit var adapter: ContactAdapter
    private lateinit var listContact:ArrayList<DataContact>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        onAttach()
        initView()
    }

    private fun initView(){
        initToolbar(toolbarHome,
            "Kelola Kontak",
            ContextCompat.getDrawable(this, R.drawable.ic_close)!!
        )

        rvContact.apply {
            layoutManager = LinearLayoutManager(this@HomeActivity)
            setHasFixedSize(true)
        }

        adapter = ContactAdapter(listContact,
            object : ContactAdapter.ContactListener {
                override fun onClick(item: DataContact) {

                } })
        rvContact.adapter = adapter

    }

    private fun onAttach()
    {

    }
}