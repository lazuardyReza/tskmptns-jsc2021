package com.reza.lazuardy.testjsc.model.networking

import com.reza.lazuardy.testjsc.model.StatusResponse


interface DelegateAPIFailed {
    fun onFailed(isNeedToShow: Boolean, statusResponse: StatusResponse)
}
