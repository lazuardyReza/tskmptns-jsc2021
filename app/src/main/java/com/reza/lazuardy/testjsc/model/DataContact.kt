package com.reza.lazuardy.testjsc.model

import kotlinx.android.parcel.Parcelize
import android.os.Parcelable
import eu.amirs.JSON
import id.damcorp.baseapp.ekstensions.getKey
import id.damcorp.baseapp.ekstensions.getList
import id.damcorp.baseapp.ekstensions.getString


@Parcelize
data class DataContact (
    var gender:String="",
    var nameModel:NameModel=NameModel(),
    var locationModel: LocationModel=LocationModel(),
    var email:String="",
    var dobModel: DOBModel = DOBModel(),
    var phone: String = "",
    var cell: String = "",
    var idModel: IdModel = IdModel(),
    var pictureModel: PictureModel = PictureModel()
) : Parcelable {
    constructor(data: JSON) : this() {
        this.gender = data.getString("gender")
        this.nameModel =  NameModel(data.getKey("name"))
        this.locationModel =  LocationModel(data.getKey("location"))
        this.email = data.getString("email")
        this.dobModel =  DOBModel(data.getKey("dob"))
        this.phone = data.getString("phone")
        this.cell = data.getString("cell")
        this.idModel =  IdModel(data.getKey("id"))
        this.pictureModel = PictureModel(data.getKey("picture"))

    }
}