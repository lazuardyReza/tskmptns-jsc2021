package com.reza.lazuardy.testjsc.model

import kotlinx.android.parcel.Parcelize
import android.os.Parcelable
import eu.amirs.JSON
import id.damcorp.baseapp.ekstensions.getKey
import id.damcorp.baseapp.ekstensions.getList
import id.damcorp.baseapp.ekstensions.getString


@Parcelize
data class CoordinateModel (
    var latitude:String="",
    var longitude:String=""
) : Parcelable {
    constructor(data: JSON) : this() {
        this.latitude = data.getString("latitude")
        this.longitude = data.getString("longitude")

    }
}